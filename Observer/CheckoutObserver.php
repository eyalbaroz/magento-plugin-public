<?php
namespace Envite\EnviteChat\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;

class CheckoutObserver implements ObserverInterface
{
    protected $_order;
    protected $customerSession;
    protected $_productRepository;
    public function __construct(
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Model\ProductRepository $productRepository
    ) {
        $this->_order = $order;
        $this->customerSession = $customerSession;
        $this->_productRepository = $productRepository;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $orderIds = $observer->getEvent()->getOrderIds();
        $orderId = $orderIds[0];
        $order = $this->_order->load($orderId);

        $products = array();
        $products['order_id'] = $orderId;
        $products['grand_total'] = number_format($order->getGrandTotal(),2); // Total price after discount amount
        $products['discount'] = number_format($order->getDiscountAmount(),2); // Discount amount
        $products['type'] = 'paid'; // Order Type
        $products['currency'] = 'USD';  // Currency Type
        $products['items'] = array();
        foreach($order->getAllItems() as $item){

            $productCollections = array();
            $productCollections['id'] = $item->getProductId();
            $productCollections['total_price']=$item->getQtyOrdered() * number_format($item->getPrice(), 2);
            $productCollections['price'] = number_format($item->getPrice(), 2);
            $productCollections['qty'] = number_format($item->getQtyOrdered(),2);
            $productCollections['name'] = $item->getName();
            $product = $this->_productRepository->getById($item->getProductId());//Fetch item by id from repository
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();//Initial objectManager
            $imageHelper  = $objectManager->get('\Magento\Catalog\Helper\Image');//Initial image module
            $image_url = $imageHelper->init($product, 'small_image')->setImageFile($product->getSmallImage())->resize(200, 200)->getUrl();//Get image real url
            $productCollections['image'] = $image_url;
            $productCollections['url'] = $product->getProductUrl();//Fetch product url from objectManager
            array_push($products['items'],$productCollections);
        }
        //Set session to pass script.phtml file
        $this->customerSession->setEnviteSubmitOrder($products);
    }

}
