<h1>=== Envite ===</h1>
<pre>
Contributors: envite
Plugin Name: Envite
Plugin URI: https://envite.shop
Tags: magento, magento 2, chat, messenger, engagement, marketing, sharing, commenting
Author URI: https://envite.shop
Author: Envite LLC
Requires at least: 4.0
Requires PHP: >=7.2 && <=7.4
Tested up to: 7.2
Stable tag: 1.1.0
Version: 1.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Socializing eCommerce – Bring the chat INTO your site! More Traffic, Better UX and Higher Conversion.
</pre>
== Description ==

<p>Socialize your online shop by adding this new plug-in that enables users to invite their friends and family to shop together online!</p> 
<p>Get your site a social boost! Enhance your traffic and get the user experience to new levels! Completely compatible on MOBILE and WEB!</p>

<p>Shopping is a decision making process and everyone seeks advice before they buy, they do it anyway using external chat solutions like whatsapp, messenger or the old way of calling. Make it easier for them, provide them an enhanced user experience just by adding Envite innovative plug-in and increase your conversion rates! 
Link your brand name to innovation! Make an announcement about the functionality of your new Envite chat and your users will never believe they were shopping online without it!</p> 
So, what are you waiting for? Send me an Envite! ;-)

Product functions:

* One-on-One Chat: Shared experience for 2! Chat, Share products and vote!
* Group Chat: Shared experience for more than 2! Chat, create groups, run product polls!
* Influencers Forums: Drive influencers to host a shopping together experience with their followers!
* Marketing Events: Hold innovative online events with special promotions, shop with a celebrity or a professional stylist, get your users to vote on their choice of product, collection, or any other marketing initiative!

Product features:

* Envite chat interface: Striking chat functionality compatible for mobile and web
* Instant Plug-in: Add-on to host merchant compatible with WordPress and WooCommerce. Coming soon also for Wix, Magento and Shopify
* Cart & Thank-you page integration: Analytics for all funnel faces
* Quick product share: Sharing functionality button integrated in all product pages enabling users to easily add a product into the Envite chat!
* Merchant Back-Office: Plugin settings, customizable white labeling, analysis dashboard and marketing functions


== Installation ==

1)	Install the Envite Plug-in into your Magento Shop with composer

<code>composer require envite/envite-chat</code>

2)	Go to https://envite.shop and signup or go directly to https://admin.envite.shop/signup

3)	On the envite Admin, follow the Account setup steps as follows
*	Complete your account details and billing information
*	Add your website URL
*	Customize your Envite chat Look&Feel
*	Copy your Envite Site ID

Next Steps are in magento

4.	Go back to Magento admin, click on the system button and next click on custom variables in Other settings section

5. Click add new variable
* paste in "variable code" `envite_site`
* paste in "variable name" `envite_site`
* paste in "Variable HTML Value" `html code from envite`
* paste in "Variable Plain Value" `envite site id for example: Pduag7T0me9dLr9Rg7KD`
* Click save button

<h2>you are ready to go!</h2>

== Upgrade Notice ==

None

== Screenshots ==

1. User web UI
2. User web UI
3. User web UI
4. User web UI
5. User mobile UI
6. User mobile UI
7. User mobile UI
8. User mobile UI
9. Admin Interface
10. Admin Interface
11. Admin Interface

== Changelog ==

1.1.0 – First version.

== Frequently Asked Questions ==

Q: How do I get started?
A: Currently the plug-in has free first month trial. Just register and follow the onboarding steps to get your Envite chat integrated. You can register through https://envite.shop/ or directly through this link https://admin.envite.shop/signup

Q: How can Envite increase my traffic?
A: Envite plug-in get enables your users to invite their friend and family to your online shop and chat about your products, therefore your own users will actively boost your organic traffic, constantly bringing you quality leads.

Q: How can Envite influence my conversion rate?
A: Envite plug-in will help you keep your users on your shop longer while chatting about your products with their friends, keeping the conversation inside your site can help them reach their buying decision faster and consequently improve your conversion rates.

Q: How do I market Envite in my shop?
A: After you register to Envite, you can get a marketing link in your Admin Account Setup and integrate it in banners, posts, emails or anyplace of your choice, that way you can easily promote your new Envite chat functionality. 

Q: Can I customize the Envite plug-in Look&Feel?
A: Yes, once you register your site, you will be able to customize the text, chat and theme colors through the Account Setup on your admin interface.

Q: Does Envite facilitate reports on the user’s activity?
A: Yes, once you register to Envite, you will get an Admin interface that includes a comprehensive dashboard to follow-up on your funnel performance from visit to sales.

