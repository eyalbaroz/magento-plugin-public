<?php
namespace Envite\EnviteChat\Block;

class EnviteSiteId extends \Magento\Framework\View\Element\Template {

    protected $_varFactory;

    public function __construct(\Magento\Variable\Model\VariableFactory $varFactory,
                                \Magento\Framework\View\Element\Template\Context $context){
        $this->_varFactory = $varFactory;
        parent::__construct($context);
    }

    public function getPlainCode(){
        $model = $this->_varFactory->create();
        $model->loadByCode('envite_site_id');
        return $model->getValue('text');
    }
}


